package ru.lmd.testcalculator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        int firstOperator = getIntent().getExtras().getInt("firstOperator");
        int secondOperator = getIntent().getExtras().getInt("secondOperator");
        int result = getIntent().getExtras().getInt("result");


        TextView counterView = findViewById(R.id.first_operand);
        counterView.setText(firstOperator + "");

        counterView = findViewById(R.id.second_operand);
        counterView.setText(secondOperator + "");

        counterView = findViewById(R.id.result);
        counterView.setText(result + "");
    }
}
