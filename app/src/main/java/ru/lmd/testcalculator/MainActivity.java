package ru.lmd.testcalculator;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText firstOperand;
    EditText secondOperand;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstOperand = (EditText) findViewById(R.id.first_operand);
        secondOperand = (EditText) findViewById(R.id.second_operand);
    }

    public void adding(View view) {
        int num1, num2, result;


        num1 = Integer.parseInt(firstOperand.getText().toString());
        num2 = Integer.parseInt(secondOperand.getText().toString());
        result = num1 + num2;

        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("firstOperator", num1);
        intent.putExtra("secondOperator", num2);
        intent.putExtra("result", result);

        startActivity(intent);
    }
}
